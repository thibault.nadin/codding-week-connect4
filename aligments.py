from board import *
import numpy as np
inf = 1e9

def identifiant(plateau):
    s = 0
    for col in range(plateau.width):
        for row in range(plateau.height):
            s += (1 + plateau.data[row,col])*3**(col + plateau.width * row)
    return s

def nb_aligns(board, k, color):
    #detection alignement ligne
    tab_lign = np.zeros((board.height, board.width - k+1))
    for i in range(k):
        tab_lign += board.data[:, i:board.width-k+i+1]
    mask_lign = (tab_lign == k*color)
    #detection alignement colonne
    tab_col = np.zeros((board.height - k, board.width))
    for i in range(k):
        tab_col += board.data[i:board.height-k+i,:]
    mask_col = (tab_col == k*color)
    #diagonale pente positive
    tab_diag_up = np.zeros((board.height - k+1, board.width - k+1))
    for i in range(k):
        tab_diag_up += board.data[i:board.height-k+1+i, i:board.width-k+1+i]
    mask_diag_up = (tab_diag_up == k*color)
    #diagonale pente négative
    tab_diag_down = np.zeros((board.height - k+1, board.width - k+1))
    for i in range(k):
        tab_diag_down += board.data[k-1-i:board.height-i, i:board.width-k+1+i]
    mask_diag_down = (tab_diag_down == k*color)
    return len(tab_lign[mask_lign]) + len(tab_col[mask_col]) + len(tab_diag_up[mask_diag_up]) + len(tab_diag_down[mask_diag_down])

def winning_aligns(board, color):
    nb_winning_position = 0
    #colonne
    tab = np.zeros((board.height, board.width - board.nb_align_to_win+1))
    for i in range(board.nb_align_to_win):
        tab += board.data[:, i:board.width-board.nb_align_to_win+i+1]
    if np.max(tab*color)==(board.nb_align_to_win-1):
        nb_winning_position +=1
    #ligne
    tab = np.zeros((board.height - board.nb_align_to_win+1, board.width))
    for i in range(board.nb_align_to_win):
        tab += board.data[i:board.height-board.nb_align_to_win+i+1,:]
    if np.max(tab*color)==(board.nb_align_to_win-1):
        nb_winning_position +=1
    #diagonale pente positive
    tab = np.zeros((board.height - board.nb_align_to_win+1, board.width - board.nb_align_to_win+1))
    for i in range(board.nb_align_to_win):
        tab += board.data[i:board.height-board.nb_align_to_win+i+1, i:board.width-board.nb_align_to_win+i+1]
    if np.max(tab*color)==(board.nb_align_to_win-1):
        nb_winning_position +=1
    #diagonale pente négative
    tab = np.zeros((board.height - board.nb_align_to_win+1, board.width - board.nb_align_to_win+1))
    for i in range(board.nb_align_to_win):
        tab += board.data[board.nb_align_to_win-i-1:board.height-i, i:board.width-board.nb_align_to_win+i+1]
    if np.max(tab*color)==(board.nb_align_to_win-1):
        nb_winning_position +=1
    return nb_winning_position

def evaluation(board, good_player):
    assert good_player!=0
    b, winner, _ = board.is_terminal()
    if b and winner==good_player:
        s = inf
    elif b and winner==-1 * good_player:
        s = -inf
    elif b:
        print("nul")
        s = 0
    else:
        s = nb_aligns(board, board.nb_align_to_win-1, good_player) + 100 * nb_aligns(board, board.nb_align_to_win-2, good_player)
        s += -1 * nb_aligns(board, board.nb_align_to_win-1, -1 * good_player) - 100 * nb_aligns(board, board.nb_align_to_win-2, -1 * good_player)
        s += winning_aligns(board, good_player)*100000
        s += -1*winning_aligns(board, -1 * good_player)*100000
    return s

