from pytest import *
from board import *
from archives.parameters import *
from aligments import *
import pytest

##test creation grille vide :
def test_grille_vide():
    board = Board()
    assert board.data.shape == (board.height, board.width)
    assert board.data.all()==0

test_grille_vide()

##test fonction copy_board
def test_copy_board():
    board = Board()
    new_board = board.copy_board()
    assert new_board.data.shape == board.data.shape
    assert (new_board.data - board.data).all()==0
    board.data[0, 0] = 1
    assert (new_board.data - board.data).any()!=0

test_copy_board()

#test terminaison :
board_1 = Board()
board_1.width, board_1.height, board_1.players, board_1.max_deep, board_1.nb_align_to_win = 8, 7, {-1:"human", 1:"human"}, 3, 4
board_1.data = np.array([[-1., 1., -1., 1., 0., 0., 0., 0.],
                [1., -1., 1., -1., 0., 0., 0., 0.],
                [0., 0., -1., 1., 0., 0., 0., 0.],
                [0., 0., 0., -1., 0., 0., 0., 0.],
                [0., 0., 0., 0., 0., 0., 0., 0.],
                [0., 0., 0., 0., 0., 0., 0., 0.],
                [0., 0., 0., 0., 0., 0., 0., 0.]])
board_2 = Board()
board_2.width, board_2.height, board_2.players, board_2.max_deep, board_2.nb_align_to_win = 8, 7, {-1:"human", 1:"human"}, 3, 4
board_2.data = np.array([
    [ 0.,  0.,  1.,  1., -1., -1., -1.,  1.],
    [ 0.,  0.,  1., -1., -1., -1., -1.,  0.],
    [ 0.,  0.,  0.,  1.,  1., -1.,  0.,  0.],
    [ 0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.],
    [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
    [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
    [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.]
])
board_3 = Board()
board_3.data = np.array([[0, -1, -1, 1, 1, -1, -1, -1, 1, 0],
                    [0, 1, 1, 1, -1, -1, 1, 0, 0, 0],
                    [0, 1, -1, -1, -1, 1, 0, 0, 0, 0],
                    [0, 0, -1, 1, -1, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
board_3.width, board_3.height, board_3.players, board_3.max_deep, board_3.nb_align_to_win = 10, 8, {-1:"human", 1:"human"}, 3, 4

def test_is_terminal_board():
    assert board_1.is_terminal() == (True, -1, [(0, 0), (1, 1), (2, 2), (3, 3)])
    board_1.data *= -1
    assert board_1.is_terminal() == (True, 1, [(0, 0), (1, 1), (2, 2), (3, 3)])
    board_1.data = np.flip(board_1.data, axis=1)
    assert board_1.is_terminal() == (True, 1, [(7, 0), (6, 1), (5, 2), (4, 3)])
    assert board_2.is_terminal() == (True, -1, [(3, 1), (4, 1), (5, 1), (6, 1)])
    board_2.data *= -1
    assert board_2.is_terminal() == (True, 1, [(3, 1), (4, 1), (5, 1), (6, 1)])
    board_2.data = np.flip(board_2.data, axis=1)
    assert board_2.is_terminal() == (True, 1, [(1, 1), (2, 1), (3, 1), (4, 1)])
    board_2.data[1,1]=0
    assert board_2.is_terminal() == (False, None, None)
    assert board_3.is_terminal() == (True, -1, [(5, 0), (4, 1), (3, 2), (2, 3)])
    board_3.data *= -1
    assert board_3.is_terminal() == (True, 1, [(5, 0), (4, 1), (3, 2), (2, 3)])
    board_3.data = np.flip(board_3.data, axis=1)
    assert board_3.is_terminal() == (True, 1, [(4, 0), (5, 1), (6, 2), (7, 3)])
    board_3.data[0,4]=0
    assert board_3.is_terminal() == (False, None, None)
    board_3.data[0,4]=1
test_is_terminal_board()

#test nb_aligns :
def test_nb_aligns():
    #board_1.display_board()
    assert nb_aligns(board_1, 4, -1)==0
    assert nb_aligns(board_1, 3, -1)==1
    assert nb_aligns(board_1, 2, -1)==4
    assert nb_aligns(board_1, 4, 1)==1
    assert nb_aligns(board_1, 3, 1)==2
    assert nb_aligns(board_1, 2, 1)==6
    #board_2.display_board()
    assert nb_aligns(board_2, 3, -1)==0
    assert nb_aligns(board_2, 3, 1)==3
    #board_3.display_board()
    assert nb_aligns(board_3, 3, -1)==2
    assert nb_aligns(board_3, 4, 1)==1
    assert nb_aligns(board_3, 2, -1)==10
    assert nb_aligns(board_3, 2, 1)==16

test_nb_aligns()

def test_winning_aligns():
    #board_1.display_board()
    assert winning_aligns(board_1, -1)==1
    assert winning_aligns(board_1, 1)==0
    board_1.data[3,4]=0
    #board_1.display_board()
    assert winning_aligns(board_1, -1)==1
    assert winning_aligns(board_1, 1)==1
    #board_2.display_board()
    assert winning_aligns(board_2, -1)==0
    assert winning_aligns(board_2, 1)==1
    #board_3.display_board()
    assert winning_aligns(board_3, -1)==2
    assert winning_aligns(board_3, 1)==0

test_winning_aligns()

print("All tests done")