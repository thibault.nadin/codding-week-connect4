#avec du RL
from archives.parameters import *
from board import *
import time
import random as rd
import json

memoire_board = {}

def identifiant(plateau):
    s = 0
    for col in range(width):
        for row in range(len(plateau.data[col])):
            s += (1 + plateau.data[col][row])*3**(col + width * row)
    return s

def add_to_a_colomn(game_space, col):
    row = len(game_space.data[col])
    if row < height:
        game_space.data[col].append(game_space.player)
        game_space.player = (game_space.player + 1)%2

def entrainer(time_limit, alpha):
    t0 = time.time()
    s = 1
    while time.time() - t0 < time_limit:
        if s%100==0:
            tf = open("base_coup.json", "w")
            json.dump(memoire_board, tf)
            tf.close()
        s+=1
        print(s, time.time() - t0, flush=True)
        game_space = Board()
        boards_0 = []
        boards_1 = []
        while not game_space.is_terminal()[0]:
            if rd.random() < alpha: #avec une proba alpha on fait quelque chose de complétement aleatoire
                alea = rd.randint(0, width-1)
                while len(game_space.data[alea])==height:
                    alea = rd.randint(0, width-1)
                add_to_a_colomn(game_space, alea)
            else:#on joue le meilleur coup si il existe
                coups_possibles = []
                for i in range(width):
                    if len(game_space.data[i])<height:
                        coups_possibles.append(i)
                rd.shuffle(coups_possibles)
                plateaux_possibles = []
                valeur_plateaux = []
                for coup in coups_possibles:
                    new_board = game_space.copy_board()
                    add_to_a_colomn(new_board, coup)
                    plateaux_possibles.append(new_board)
                    if identifiant(new_board) in memoire_board:
                        if game_space.player==0:
                            valeur_plateaux.append(memoire_board[identifiant(new_board)])
                        else:
                            valeur_plateaux.append(-1*memoire_board[identifiant(new_board)])
                    else:
                        valeur_plateaux.append(0)
                def max(a,b):
                    imax = 0
                    max = -inf
                    for i in range(len(a)):
                        if a[i]>max:
                            max = a[i]
                            imax = i
                    return b[imax]
                best_plateau = max(valeur_plateaux, plateaux_possibles)
                if game_space.player==0:
                    boards_0.append(best_plateau)
                else:
                    boards_1.append(best_plateau)
        _,w,_ = game_space.is_terminal()
        if w==0:
            for board in boards_0:
                if identifiant(board) in memoire_board:
                    memoire_board[identifiant(board)] +=1
                else:
                    memoire_board[identifiant(board)] = 1
            for board in boards_1:
                if identifiant(board) in memoire_board:
                    memoire_board[identifiant(board)] +=1
                else:
                    memoire_board[identifiant(board)] = 1
        if w==1:
            for board in boards_0:
                if identifiant(board) in memoire_board:
                    memoire_board[identifiant(board)] += -1
                else:
                    memoire_board[identifiant(board)] = -1
            for board in boards_1:
                if identifiant(board) in memoire_board:
                    memoire_board[identifiant(board)] += -1
                else:
                    memoire_board[identifiant(board)] = -1

def best_column_rl(game_space):
    coups_possibles = []
    for i in range(width):
        if len(game_space.data[i])<height:
            coups_possibles.append(i)
    rd.shuffle(coups_possibles)
    plateaux_possibles = []
    valeur_plateaux = []
    for coup in coups_possibles:
        new_board = game_space.copy_board()
        add_to_a_colomn(new_board, coup)
        plateaux_possibles.append(new_board)
        if identifiant(new_board) in memoire_board:
            if game_space.player==0:
                valeur_plateaux.append(memoire_board[identifiant(new_board)])
            else:
                valeur_plateaux.append(-1*memoire_board[identifiant(new_board)])
        else:
            valeur_plateaux.append(0)
    def max(a,b):
        imax = 0
        max = -inf
        for i in range(len(a)):
            if a[i]>max:
                max = a[i]
                imax = i
        return b[imax]
    best_colomn = max(valeur_plateaux, coups_possibles)
    return best_colomn

if __name__ == "__main__":
    entrainer(24*3600, 0.1)
    print(memoire_board)
    tf = open("base_coup.json", "w")
    json.dump(memoire_board, tf)
    tf.close()
else:
    tf = open("base_coup2.json", "r")
    new_dict = json.load(tf)
    #print(new_dict)
    tf.close()
