import pygame
import numpy as np

def import_parameters():
    try:
        f = open("parameters.txt", "r")
        parameters = f.read().split(';')
        f.close()
        width = int(parameters[0])
        height = int(parameters[1])
        players_1 = parameters[2]
        players_2 = parameters[3]
        max_deep = int(parameters[4])
        nb_align_to_win = int(parameters[5])
    except:
        width, height, players_1, players_2, max_deep, nb_align_to_win = 8, 7, "ai", "human", 4, 4
    return width, height, {-1 : players_1, 1 : players_2}, max_deep, nb_align_to_win

def save_parameters(width, height, players, max_deep, nb_align_to_win):
    '''
    Sauvegarde dans un fichier txt des paramètres
    '''
    string = str(width) + ";" + str(height) + ";" + str(players[-1]) + ";" + str(players[1]) + ";" + str(max_deep) + ";" + str(nb_align_to_win)
    f = open("parameters.txt", "w")
    f.write(string)
    f.close()

def setting_windows():
    pygame.init()

    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    GREY = (150, 150, 150)

    #parametres precedents
    width, height, players, max_deep, nb_align_to_win = import_parameters()
    players_1 = players[-1]
    players_2 = players[1]

    def dessiner_bouton(bouton_area, x, y, bouton_width, bouton_height, text):
        pygame.draw.rect(bouton_area, BLACK, (x, y, bouton_width, bouton_height))
        pygame.draw.rect(bouton_area, GREY, (x+2, y+2, bouton_width-4, bouton_height-4))
        font = pygame.font.SysFont(None, 15)
        text = font.render(text, True, BLACK)
        bouton_area.blit(text, (x + (bouton_width/2 - text.get_width()/2), y + (bouton_height/2 - text.get_height()/2)))

    def afficher_parametres(bouton_area, width, height):
        font = pygame.font.SysFont(None, 30)
        text_width = font.render(f"Largeur : {width}", True, BLACK)
        bouton_area.blit(text_width, (50, 50))
        text_height = font.render(f"Hauteur : {height}", True, BLACK)
        bouton_area.blit(text_height, (50, 100))
        text_height = font.render(f"Joueur 1 : {players_1}", True, BLACK)
        bouton_area.blit(text_height, (50, 150))
        text_height = font.render(f"Joueur 2 : {players_2}", True, BLACK)
        bouton_area.blit(text_height, (50, 200))
        text_height = font.render(f"Nb alignements : {nb_align_to_win}", True, BLACK)
        bouton_area.blit(text_height, (50, 250))

    bouton_width_window_parameters = 500
    bouton_height_window_parameters = 300
    window_parameters = pygame.display.set_mode((bouton_width_window_parameters, bouton_height_window_parameters))
    pygame.display.set_caption("Réglage des paramètres")

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                save_parameters(width, height, {-1 : players_1, 1 : players_2}, max_deep, nb_align_to_win)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                x, y = event.pos
                if 300 <= x <= 350 and 50 <= y <= 75:
                    width += -1
                elif 350 <= x <= 400 and 50 <= y <= 75:
                    width += 1
                elif 300 <= x <= 350 and 100 <= y <= 125:
                    height += -1
                elif 350 <= x <= 400 and 100 <= y <= 125:
                    height += 1
                elif 300 <= x <= 350 and 150 <= y <= 175:
                    players_1 = 'ai'
                elif 350 <= x <= 400 and 150 <= y <= 175:
                    players_1 = 'human'
                elif 300 <= x <= 350 and 200 <= y <= 225:
                    players_2 = 'ai'
                elif 350 <= x <= 400 and 200 <= y <= 225:
                    players_2 = 'human'
                elif 300 <= x <= 350 and 250 <= y <= 275:
                    nb_align_to_win += -1
                elif 350 <= x <= 400 and 250 <= y <= 275:
                    nb_align_to_win += 1
                width = np.clip(width, 6, 10)
                height = np.clip(height, 6, 10)
                nb_align_to_win = np.clip(nb_align_to_win, 3, 6)
                players = {-1 : players_1, 1 : players_2}

        # Effacement de l'écran
        window_parameters.fill(WHITE)

        # Dessin des boutons
        #Width
        dessiner_bouton(window_parameters, 300, 50, 50, 25, "-1")
        dessiner_bouton(window_parameters, 350, 50, 50, 25, "+1")
        #height
        dessiner_bouton(window_parameters, 300, 100, 50, 25, "-1")
        dessiner_bouton(window_parameters, 350, 100, 50, 25, "+1")
        #player -1
        dessiner_bouton(window_parameters, 300, 150, 50, 25, "AI")
        dessiner_bouton(window_parameters, 350, 150, 50, 25, "Human")
        #player 1
        dessiner_bouton(window_parameters, 300, 200, 50, 25, "AI")
        dessiner_bouton(window_parameters, 350, 200, 50, 25, "Human")
        #nb_aligns_to_win
        dessiner_bouton(window_parameters, 300, 250, 50, 25, "-1")
        dessiner_bouton(window_parameters, 350, 250, 50, 25, "+1")

        # Affichage des paramètres
        afficher_parametres(window_parameters, width, height)

        # Rafraîchissement de l'écran
        pygame.display.flip()

