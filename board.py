import pygame
from fenetre_para import import_parameters
import time
from copy import deepcopy
import numpy as np
from aligments import *
inf = 1e9

class Board:
    def __init__(self, ai_func = None):
        '''
        data : matrice du jeu :
            premier indice : colonne sélectionée
            deuxieme indice : ligne sélectionnée (vide s'il n'y a pas de jeton)
        player : joueur dont c'est le tour 0 pour J1 ; 1 pour J2
        ai_function : la fonction qui donne les coups à jouer si on joue contre une IA
        '''
        pygame.init()
        self.width, self.height, self.players, self.max_deep, self.nb_align_to_win = import_parameters()
        self.data = np.zeros((self.height, self.width))
        self.player = -1 #c'est au premier joueur de commencer
        self.ai_function = ai_func

    
    def copy_board(self):
        '''
        Entrée : 
            un objet de type board
        Sortie :
            une copie de cet objet
            le numéro du vaincoeur (0 pour J1 ; 1 pour J2)
            les coordonnées des point de la combinaison réalisant l'allignement
        '''
        new = Board()
        new.width, new.height, new.players, new.max_deep, new.nb_align_to_win = self.width, self.height, self.players, self.max_deep, self.nb_align_to_win
        new.data = deepcopy(self.data)
        new.player = self.player
        new.ai_function = self.ai_function
        return new
    
    def is_terminal(self):
        #detection alignement ligne
        tab = np.zeros((self.height, self.width - self.nb_align_to_win+1))
        for i in range(self.nb_align_to_win):
            tab += self.data[:, i:self.width-self.nb_align_to_win+i+1]
        if np.max(tab)==self.nb_align_to_win:
            iy, ix = np.unravel_index(np.argmax(tab, axis=None), tab.shape)
            return True, 1, [(ix+i, iy) for i in range(self.nb_align_to_win)]
        if np.min(tab)==-self.nb_align_to_win:
            iy, ix = np.unravel_index(np.argmin(tab, axis=None), tab.shape)
            return True, -1, [(ix+i, iy) for i in range(self.nb_align_to_win)]
        #detection alignement colonne
        tab = np.zeros((self.height - self.nb_align_to_win+1, self.width))
        for i in range(self.nb_align_to_win):
            tab = tab + self.data[i:self.height-self.nb_align_to_win+i+1,:]
        if np.max(tab)==self.nb_align_to_win:
            iy, ix = np.unravel_index(np.argmax(tab, axis=None), tab.shape)
            return True, 1, [(ix,iy+i) for i in range(self.nb_align_to_win)]
        if np.min(tab)==-self.nb_align_to_win:
            iy, ix = np.unravel_index(np.argmin(tab, axis=None), tab.shape)
            return True, -1, [(ix,iy+i) for i in range(self.nb_align_to_win)]
        #diagonale pente positive
        tab = np.zeros((self.height - self.nb_align_to_win+1, self.width - self.nb_align_to_win+1))
        for i in range(self.nb_align_to_win):
            tab += self.data[i:self.height-self.nb_align_to_win+1+i, i:self.width-self.nb_align_to_win+1+i]
        if np.max(tab)==self.nb_align_to_win:
            iy, ix = np.unravel_index(np.argmax(tab, axis=None), tab.shape)
            return True, 1, [(ix+i,iy + i) for i in range(self.nb_align_to_win)]
        if np.min(tab)==-self.nb_align_to_win:
            iy, ix = np.unravel_index(np.argmin(tab, axis=None), tab.shape)
            return True, -1, [(ix+i,iy + i) for i in range(self.nb_align_to_win)]
        #diagonale pente négative
        tab = np.zeros((self.height - self.nb_align_to_win+1, self.width - self.nb_align_to_win+1))
        for i in range(self.nb_align_to_win):
            tab += self.data[self.nb_align_to_win-1-i:self.height-i, i:self.width-self.nb_align_to_win+1+i]
        if np.max(tab)==self.nb_align_to_win:
            iy, ix = np.unravel_index(np.argmax(tab, axis=None), tab.shape)
            return True, 1, [(ix-i+self.nb_align_to_win-1,iy + i) for i in range(self.nb_align_to_win)]
        if np.min(tab)==-self.nb_align_to_win:
            iy, ix = np.unravel_index(np.argmin(tab, axis=None), tab.shape)
            return True, -1, [(ix-i+self.nb_align_to_win-1,iy + i) for i in range(self.nb_align_to_win)]
        if np.sum(np.absolute(self.data))==self.height*self.width:
            return True, None, None
        return False, None, None

    def display_board(self):
        '''
        Affiche le plateau et le fait évoluer selon les coup des joueurs
        '''
        # Dimensions de l'écran et des cases
        square_size = 80
        line_size = 2
        button_space_height = 0 # on a un espace disponible pour éventuellement ajouter des informations sur la partie supérieure de l'écran
        screen_width = square_size * self.width
        screen_height = square_size * self.height + button_space_height
        
        # Couleurs
        white = (10, 10, 10)
        black = (255, 255, 255)

        # Création de l'écran
        screen = pygame.display.set_mode((screen_width, screen_height))
        pygame.display.set_caption("Puissance4")

        # Dessiner les cases du plateau
        for row in range(self.height):
            for col in range(self.width):
                rect = pygame.Rect(col * square_size, row * square_size + button_space_height, square_size, square_size)
                pygame.draw.rect(screen, black, rect)
                rect = pygame.Rect(col * square_size + line_size, row * square_size + line_size + button_space_height, square_size - 2*line_size, square_size-2*line_size)
                pygame.draw.rect(screen, white, rect)

        # Dessiner les cercles préplacés :
        for col in range(self.width):
            for row in range(self.height):
                if self.data[row, col]==1:#bleu
                    color = (0,0,255)
                    pygame.draw.circle(screen, color, (col * square_size + square_size // 2, (self.height -1 - row) * square_size + square_size // 2 + button_space_height), square_size // 3)
                elif  self.data[row, col]==-1:#rouge
                    color = (255,0,0)
                    pygame.draw.circle(screen, color, (col * square_size + square_size // 2, (self.height -1 - row) * square_size + square_size // 2 + button_space_height), square_size // 3)
        
        # Mettre à jour l'écran
        pygame.display.flip()

        #fonction qui ajoute un jeton à la colonne séléctionné
        def add_to_a_column(col):
            '''
            Entrée :
                l'indice d'une colonne
            Fonction :
                si la colonne n'est pas pleine
                ajoute un jeton dans la colonne correspondante
                met à jour l'écran
            '''
            row = int(np.sum(np.absolute(self.data[:,col])))
            if row < self.height:
                self.data[row,col] = self.player
                self.player = -1 * self.player
                if self.data[row,col]==1:#bleu
                    color = (0,0,255)
                else:#rouge
                    color = (255,0,0)
                pygame.draw.circle(screen, color, (col * square_size + square_size // 2, (self.height -1 - row) * square_size + square_size // 2 + button_space_height), square_size // 3)
                pygame.display.flip()
        
        def show_final(combi):
            '''
            Entrée :
                les coordonnées de la combinaison qui fait gagner
            Fonction :
                affiche à l'écran cette combinaison
            '''
            if combi is not None:
                for col, row in combi:
                    color = (255,255,255)
                    pygame.draw.circle(screen, color, (col * square_size + square_size // 2, (self.height -1 - row) * square_size + square_size // 2 + button_space_height), square_size // 4)
                pygame.display.flip()

        #attendre de fermer la fenètre
        running = True
        finished = False
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    if not(self.is_terminal()[0]):
                        np.save("unfinished_board.npy", self.data)
                    running = False
            if self.players[self.player]=="human" and not(finished):
                current_col = pygame.mouse.get_pos()[0]//square_size
                if pygame.mouse.get_pressed()[0]:#clic gauche souris
                    add_to_a_column(current_col)
                    pygame.display.flip()
                    bool, winner, combi = self.is_terminal()
                    if bool:
                        print("Vainqueur :", winner)
                        show_final(combi)
                        finished = True
                    elif self.players[-1*self.player]=="human":
                        time.sleep(0.5)
            elif not(finished):
                add_to_a_column(self.ai_function(self))
                pygame.display.flip()
                bool, winner, combi = self.is_terminal()
                if bool:
                    print("Vainqueur :", winner)
                    show_final(combi)
                    finished = True
        pygame.quit()
        return None
    
    def play_without_pygame(self):
        '''
        fait évoluer selon les coup des joueurs
        '''
        #fonction qui ajoute un jeton à la colonne séléctionné
        def add_to_a_column(col):
            pass
        pass