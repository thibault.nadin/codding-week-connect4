from board import *
from ai_player import best_column
from aligments import *
from fenetre_para import setting_windows
import os

def main():
    if os.path.exists("unfinished_board.npy"):
        game_space = Board(best_column)
        game_space.data = np.load("unfinished_board.npy")
        if np.sum(game_space.data)==0:
            game_space.player = -1
        else:
            game_space.player = 1
        os.remove("unfinished_board.npy")
    else:
        setting_windows()
        game_space = Board(best_column)
    game_space.display_board()


if __name__=="__main__":
    main()