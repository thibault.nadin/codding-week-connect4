from fenetre_para import import_parameters
from board import *
import time
import random as rd
import numpy as np
from aligments import *
inf = 1e9

def identifiant(plateau):
    s = 0
    for col in range(plateau.width):
        for row in range(plateau.height):
            s += (2 + plateau.data[row,col])*3**(col + plateau.width * row)
    return s

def sort_cols(list, width):
    '''
    Prend la liste des colonnes et trie pour avoir en premier les colonnes centrales
    '''
    values = [np.abs(i - width//2) for i in list]
    sorted_list = [e for _,e in sorted(zip(values, list))]
    return sorted_list

def children(parent_board):
    if not(parent_board.is_terminal()[0]):
        for col in sort_cols([k for k in range(parent_board.width)], parent_board.width):
            row = int(np.sum(np.absolute(parent_board.data[:,col])))
            if row<parent_board.height:
                new = parent_board.copy_board()
                new.data[row, col] = new.player
                new.player = -1*new.player
                yield new

def alpha_beta(board, deep, alpha=-100*inf, beta=100*inf):
    if board.is_terminal()[0] or deep==0:
        return evaluation(board, board.player)
    else:
        v = -100*inf
        for child in children(board):
            v = max(v, -1*alpha_beta(child, deep-1, -1*beta, -1*alpha))
            if v >=beta:
                return v
            alpha = max(alpha, v)
        return v

def best_column(board):
    t0 = time.time()
    values_and_child = [(-1 * alpha_beta(child, child.max_deep), child) for child in children(board)]
    max, best_child = values_and_child[0]
    for i in range(0,len(values_and_child)):
        if values_and_child[i][0]>max:
            max, best_child = values_and_child[i]
    print(time.time()-t0)
    for col in range(board.width):
        if np.sum(best_child.data[:,col])!=np.sum(board.data[:,col]):
            return col
    